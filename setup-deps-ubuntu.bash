#!/bin/bash

# curl -fsSL https://ldsconnect.org/setup-linux.bash | bash -c

# Not every platform has or needs sudo, gotta save them O(1)s...
sudo_cmd=""
((EUID)) && [[ -z "$ANDROID_ROOT" ]] && sudo_cmd="sudo"

NO_FAIL2BAN=${1}

echo ""
echo ""
echo "Checking for"
echo ""
echo "    * build-essential"
echo "    * rsync"
echo "    * wget"
echo "    * curl"
echo "    * pkg-config"
echo "    * node"
echo "    * jshint"
echo ""

echo "updating apt-get..."
$sudo_cmd bash -c "apt-get update -qq -y < /dev/null" > /dev/null

# fail2ban
#if [ -z "$(which fail2ban-server | grep fail2ban)" ]; then
#  if [ -z "${NO_FAIL2BAN}" ]; then
#    echo "installing fail2ban..."
#    sudo bash -c "apt-get install -qq -y fail2ban < /dev/null" > /dev/null
#  fi
#fi

# git, wget, curl, build-essential
if [ -z "$(type -p pkg-config)" ] || [ -z "$(type -p git)" ] || [ -z "$(type -p wget)" ] || [ -z "$(type -p curl)" ] || [ -z "$(type -p gcc)" ] || [ -z "$(type -p rsync)" ] || [ -z "$(type -p python)" ]
then
  echo "installing git, wget, curl, build-essential, rsync, pkg-config, python..."
  $sudo_cmd bash -c "apt-get install -qq -y git wget curl build-essential rsync pkg-config python < /dev/null" > /dev/null 2>/dev/null
fi
