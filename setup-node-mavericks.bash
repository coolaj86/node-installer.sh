#!/bin/bash

set -e
set -u
set -o pipefail

if [ -z "${my_tmp-}" ]; then
  my_tmp=$(mkdir -p)
fi
if [ -z "${PREFIX-}" ]; then
  PREFIX=""
fi

if [ -z "${NODE_PATH-}" ]; then
  node_install_path=$PREFIX/usr/local
else
  node_install_path=$(dirname $(dirname $NODE_PATH))
fi

NODEJS_VER=${1}
NODEJS_VERT=$(echo ${NODEJS_VER} | cut -c 2- | cut -d '.' -f1)

NODEJS_NAME="node"
NODEJS_BASE_URL="https://nodejs.org"


if [ $NODEJS_VERT -ge 1 ] && [ $NODEJS_VERT -lt 4 ]
then
  echo "Selecting io.js instead of node.js for this version (>= 1.0.0 < 4.0.0)"
  NODEJS_BASE_URL="https://iojs.org"
  NODEJS_NAME="iojs"
fi

# When using .pkg
#NODEJS_REMOTE="$NODEJS_BASE_URL/dist/${NODEJS_VER}/${NODEJS_NAME}-${NODEJS_VER}.pkg"
#NODEJS_PKG="/tmp/${NODEJS_NAME}-${NODEJS_VER}.pkg"

NODEJS_REMOTE="$NODEJS_BASE_URL/dist/${NODEJS_VER}/${NODEJS_NAME}-${NODEJS_VER}-darwin-x64.tar.gz"
NODEJS_PKG="$my_tmp/${NODEJS_NAME}-${NODEJS_VER}-darwin-x64.tar.gz"
NODEJS_UNTAR="$my_tmp/${NODEJS_NAME}-${NODEJS_VER}-darwin-x64"

if [ -n "${NODEJS_VER}" ]; then
  echo "installing ${NODEJS_NAME} as ${NODEJS_NAME} ${NODEJS_VER}..."
  curl -fsSL "${NODEJS_REMOTE}" -o "${NODEJS_PKG}"

  # When using .pkg
  #sudo /usr/sbin/installer -pkg "${NODEJS_PKG}" -target /

  # When using .tar.gz
  mkdir -p ${NODEJS_UNTAR}/
  tar xf "${NODEJS_PKG}" -C "${NODEJS_UNTAR}/" --strip-components=1
  rm -f ${NODEJS_UNTAR}/{LICENSE,CHANGELOG.md,README.md}

  mkdir -p "$node_install_path/" || sudo mkdir -p "$node_install_path/"
  rsync -a "${NODEJS_UNTAR}/" "$node_install_path/" || sudo rsync -a "${NODEJS_UNTAR}/" "$node_install_path/"


  chown -R $(whoami) "$node_install_path/lib/node_modules/" || sudo chown -R $(whoami) "$node_install_path/lib/node_modules/"
  chown $(whoami) "$node_install_path/bin/" || sudo chown $(whoami) "$node_install_path/bin/"
fi
