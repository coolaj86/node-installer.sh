#!/bin/bash

# Not every platform has or needs sudo, gotta save them O(1)s...
sudo_cmd=""
((EUID)) && [[ -z "${ANDROID_ROOT:-}" ]] && sudo_cmd="sudo"

set -e
set -u
set -o pipefail

if [ -z "${my_tmp-}" ]; then
  my_tmp="$(mktemp -d -t node-installer.XXXXXXXX)"
fi
if [ -z "${PREFIX-}" ]; then
  PREFIX=""
fi

if [ -z "${NODE_PATH-}" ]; then
  node_install_path=$PREFIX/usr/local
else
  node_install_path=$(dirname $(dirname $NODE_PATH))
fi

NODEJS_VER=${1}
NODEJS_VERT=$(echo ${NODEJS_VER} | cut -c 2- | cut -d '.' -f1)

NODEJS_NAME="node"
NODEJS_BASE_URL="https://nodejs.org"

if [ $NODEJS_VERT -ge 1 ] && [ $NODEJS_VERT -lt 4 ]
then
  echo "Selecting io.js instead of node.js for this version (>= 1.0.0 < 4.0.0)"
  NODEJS_BASE_URL="https://iojs.org"
  NODEJS_NAME="iojs"
fi

if [ -n "$(uname -a | grep aarch64)" ]; then
  ARCH="arm64"
elif [ -n "$(uname -a | grep 64)" ]; then
  ARCH="x64"
elif [ -n "$(uname -a | grep armv8l)" ]; then
  ARCH="arm64"
elif [ -n "$(uname -a | grep armv7l)" ]; then
  ARCH="armv7l"
elif [ -n "$(uname -a | grep armv6l)" ]; then
  ARCH="armv6l"
else
  ARCH="x86"
fi

NODEJS_REMOTE="${NODEJS_BASE_URL}/dist/${NODEJS_VER}/${NODEJS_NAME}-${NODEJS_VER}-linux-${ARCH}.tar.gz"
NODEJS_LOCAL="$my_tmp/${NODEJS_NAME}-${NODEJS_VER}-linux-${ARCH}.tar.gz"
NODEJS_UNTAR="$my_tmp/${NODEJS_NAME}-${NODEJS_VER}-linux-${ARCH}"

if [ -n "${NODEJS_VER}" ]; then
  echo "installing ${NODEJS_NAME} as ${NODEJS_NAME} ${NODEJS_VER}..."

  if [ -n "$(command -v curl 2>/dev/null | grep curl)" ]; then
    curl -fsSL ${NODEJS_REMOTE} -o ${NODEJS_LOCAL} || echo 'error downloading ${NODEJS_NAME}'
  elif [ -n "$(command -v wget 2>/dev/null | grep wget)" ]; then
    wget --quiet ${NODEJS_REMOTE} -O ${NODEJS_LOCAL} || echo 'error downloading ${NODEJS_NAME}'
  else
    echo "'wget' and 'curl' are missing. Please run the following command and try again"
    echo "    sudo apt-get install --yes curl wget"
    exit 1
  fi

  mkdir -p ${NODEJS_UNTAR}/
  # --strip-components isn't portable, switch to portable version by performing move step after untar
  tar xf ${NODEJS_LOCAL} -C ${NODEJS_UNTAR}/ #--strip-components=1
  mv ${NODEJS_UNTAR}/${NODEJS_NAME}-${NODEJS_VER}-linux-${ARCH}/* ${NODEJS_UNTAR}/
  rm -rf ${NODEJS_UNTAR}/${NODEJS_NAME}-${NODEJS_VER}-linux-${ARCH} # clean up the temporary unzip folder
  rm ${NODEJS_UNTAR}/{LICENSE,CHANGELOG.md,README.md}
  if [ -n "$(command -v rsync 2>/dev/null | grep rsync)" ]; then
    echo $sudo_cmd rsync -Krl "${NODEJS_UNTAR}/" "$node_install_path/"
    rsync -Krl "${NODEJS_UNTAR}/" "$node_install_path/" 2>/dev/null || $sudo_cmd rsync -Krl "${NODEJS_UNTAR}/" "$node_install_path/"
  else
    # due to symlink issues on Arch Linux, don't copy the share directory
    rm -rf ${NODEJS_UNTAR}/share
    echo $sudo_cmd cp -Hr "${NODEJS_UNTAR}/*" "$node_install_path/"
    cp -Hr "${NODEJS_UNTAR}"/* "$node_install_path/" 2>/dev/null || $sudo_cmd cp -Hr "${NODEJS_UNTAR}"/* "$node_install_path/"
  fi
  rm -rf "${NODEJS_UNTAR}"

  chown -R $(whoami) "$node_install_path/lib/node_modules/" 2>/dev/null || $sudo_cmd chown -R $(whoami) "$node_install_path/lib/node_modules/"
  chown $(whoami) ""$node_install_path"/bin/" 2>/dev/null || $sudo_cmd chown $(whoami) ""$node_install_path"/bin/"
fi
