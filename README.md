# STOP

The install method in this repo still has its place, but this is no longer maintained.

**Update**: Use [Webi](https://webinstall.dev) instead:

```sh
curl https://webinstall.dev/node | bash
```

# Easy Install node.js

| A [Root](https://rootprojects.org) Project |

Simple node.js installer for macOS and Linux

## QuickStart

```bash
curl -fsL bit.ly/node-installer | bash
```

*Note*: [bit.ly/node-installer](https://bit.ly/node-installer) is a redirect to <https://git.coolaj86.com/coolaj86/node-installer.sh/raw/master/install.sh>

## Screencast

[How to Setup a VPS for node.js Development](https://www.youtube.com/watch?v=ypjzi1axH2A) - [(3:06 installing node.js](https://www.youtube.com/watch?v=ypjzi1axH2A#t=186))

## Installer Options

* [x] version
* [x] install location
* [x] tools for building native modules

### Choose Version

The latest version of node is installed by default.

You can choose a specific version by defining `NODE_VERSION` in the format `vX`, `vX.Y`, or `v.X.Y.Z`:

```bash
NODE_VERSION=v10
```

Usage:

```bash
export NODE_VERSION=v10.10
curl -fsSL https://bit.ly/node-installer | bash
```

### Location

By default node will be installed to `/usr/local`, without root if possible.

You can choose a specific location by setting **both** `NPM_CONFIG_PREFIX` **and** `NODE_PATH`:

```bash
export NPM_CONFIG_PREFIX=$HOME/.local
export NODE_PATH=$NPM_CONFIG_PREFIX/lib/node_modules
curl -fsSL https://bit.ly/node-installer | bash
```

If you want to add the install location to your `PATH`, add `/bin` to the custom location used above and append it like so:

```bash
PATH=$PATH:$HOME/.local/bin
```

### Development Tools

If you plan on building or creating native node modules, you'll want to install `gcc`, `pgk-config`, and a few other required tools and niceties.

In fact, it's fairly common for modules to have both native module and pure js dependencies, so you'll probably want (or need) to install these even if you don't plan to use them directly.

* [x] curl & wget
* [x] git
* [x] rsync
* [x] xcode, brew (on macOS), build-essential (Linux)
* [x] gcc, pkg-config
* [x] pkg-config
* [x] node.js, jshint

Pass `--dev-deps` to the installer script and it will use either `brew` (on macOS) or `apt` (on Linux) to install the development tools.

```bash
curl -fsSL https://bit.ly/node-installer | bash -s -- --dev-deps
```

Or, if you don't have `curl` installed yet you can use `wget`:
```bash
wget -nv https://bit.ly/node-installer -O - | bash -s -- --dev-deps
```

### Securing your server

If you're running a node.js server on anything with a public ip address
(an "edge" server), I'd highly recommend that you also install `fail2ban` to secure ssh -
especially if you haven't switched your server to use key-only authentication (which you should also do).

See [The 15-Minute Guide to Secure VPS Access (for the Semi-Paranoid)](https://www.youtube.com/watch?v=YZzhIIJmlE0)

## Notes

* [OS X](#apple-os-x)
* [Ubuntu Linux](#ubuntu-linux)
* [Important Notes](#other-things-you-should-know)

### Apple OS X

First you need to **Install XCode Command Line Tools**

```bash
xcode-select --install
```

Then you need to **Accept the XCode License** by running any command installed by Xcode with sudo. We'll use git.

```bash
sudo git --version
```

You can scroll to the bottom by hitting shift+G (capital G).

Type `agree` and hit enter to accept the license.

Now you can install node.js

```bash
curl -fsSL https://bit.ly/node-installer -o /tmp/node-installer.sh; bash /tmp/node-installer.sh --dev-deps
```

*TODO*: Make it easier to accepting the license (automatic?)

### Ubuntu Linux

```bash
wget -nv https://bit.ly/node-installer -O /tmp/node-installer.sh; bash /tmp/node-installer.sh --dev-deps
```

### Automatic Version Detection

Fun FYI, here's how the latest version is determined:

```bash
NODE_VERSION=$(curl -fsSL https://nodejs.org/dist/index.tab | tail -n +2 | cut -f 1 | head -1 )
echo "The current node.js version is $NODE_VERSION"
```

```bash
BASE_VER="v10\\."
NODE_VERSION=$(curl -fsSL https://nodejs.org/dist/index.tab | tail -n +2 | cut -f 1 | grep $BASE_VER | head -1 )
echo "Latest node.js $BASE_VER is $NODE_VERSION"
```